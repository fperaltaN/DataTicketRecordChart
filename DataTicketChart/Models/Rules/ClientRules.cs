﻿using DataTicketChart.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models.Dao;
using WebApplication1.Models.Domain;

namespace WebApplication1.Models.Rules
{
    public class ClientRules
    {
        public List<sel_socio_new_Result> getClients()
        {
            ClientDao clients = new ClientDao();
            return clients.getClients();
        }
        public List<TblSocio> getSocioByNumber(int number)
        {
            ClientDao clientData = new ClientDao();
            return clientData.getSocioByNumber(number);
        }
    }
}